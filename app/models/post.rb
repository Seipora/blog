class Post < ApplicationRecord
 
  belongs_to :user
  validates :title, :content, presence: true
  before_validation :preval
  mount_uploader :image, ImageUploader
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings, dependent: :destroy
  has_many :ratings, dependent: :destroy

  include PgSearch::Model
  # multisearchable against: [:title, :content]
  pg_search_scope :search, 
                  against: [:title, :content],
                  associated_against: { user: :full_name,
                                        tags: :name}
  

  def tag_list
    self.tags.collect do |tag|
      tag.name
    end.join(", ")
  end

  def tag_list=(tags_string)
    tag_names = tags_string.split(",").collect{|s| s.strip.downcase}.uniq
    new_or_found_tags = tag_names.collect { |name| Tag.find_or_create_by(name: name) }
    self.tags = new_or_found_tags
  end

  private
  
  def preval
    self.title = self.title.strip if self.title
    self.content = self.content.strip if self.content
  end


end
