class PostsController < ApplicationController
  before_action :set_post, only: [:show]

  def index
    @posts = Post.all.order(:created_at)
    if params[:order] == 'latest'
      @posts = Post.all.order(created_at: :desc).paginate(page: params[:page], per_page: 5)
    elsif params[:order] == 'oldest'
      @posts = Post.all.order(created_at: :asc).paginate(page: params[:page], per_page: 5)
    else
      @posts = Post.all.paginate(page: params[:page], per_page: 5)
    end
  end

  
  def show
  end


  def search
    if params[:search].present?
      @posts= Post.search(params[:search]).paginate(page: params[:page], per_page: 5)
    end
  end

  private

  def posts_params
    params.require(:post).permit(:title, :content, :image, :tag_list)
   end

  def set_post
    @post = Post.find(params[:id])
  end
  
end