class User::PostsController < User::BaseController
  before_action :set_post, only: [:edit, :update, :show, :destroy]


  def index
    @posts = current_user.posts.all.order(:created_at)
    if params[:order] == 'latest'
      @posts = current_user.posts.order(created_at: :desc).paginate(page: params[:page], per_page: 5)
    elsif params[:order] == 'oldest'
      @posts = current_user.posts.order(created_at: :asc).paginate(page: params[:page], per_page: 5)
    else
      @posts = current_user.posts.all.paginate(page: params[:page], per_page: 5)
    end
  end

  
  def new
    @post = current_user.posts.new
  end

  def create
    @post = current_user.posts.new(posts_params)
    if @post.save
      flash[:success] = 'Successfully created a new post!'
      redirect_to user_post_path(@post)
    else
      render :new
    end
  end

  def show
  end

  def edit
  end


  def search
    if params[:search].present?
      @posts= Post.search(params[:search]).paginate(page: params[:page], per_page: 5)
    end
  end

  def update
    if @post.update(posts_params)
      flash[:success] = 'Successfully updated the post!'
      redirect_to user_post_path
    else
      render :edit
    end
  end

  def destroy
    @post.destroy
    flash[:success] = "Post was deleted"
    redirect_to user_posts_path
  end

  private

  def posts_params
    params.require(:post).permit(:title, :content, :image, :tag_list)
   end

  def set_post
    @post = Post.find(params[:id])
  end
  
end