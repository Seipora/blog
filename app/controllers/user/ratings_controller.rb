class User::RatingsController < User::BaseController
  before_action :find_post
  before_action :find_like, only: [:destroy]

  def create
    if already_liked?
      flash[:notice] = "You can't like more than once"
    else
      @post.ratings.create(user_id: current_user.id)
    end
    redirect_to request.referrer
  end

  def destroy
    if !(already_liked?)
      flash[:notice] = "Cannot unlike"
    else

      @rating.destroy
    end
    redirect_to request.referrer
  end
  
  private  
  
    def find_post
      @post = Post.find(params[:post_id])
    end

    def already_liked?
      Rating.where(user_id: current_user.id, post_id:
      params[:post_id]).exists?
    end

    def find_like
      @rating = @post.ratings.find(params[:id])
    end

end
