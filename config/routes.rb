Rails.application.routes.draw do
  devise_for :users
  root to: 'posts#index'
  resources :tags
  get '/search' => 'posts#search', :as => 'search_page'
  resources :posts, only: [:index, :show, :search]

  namespace :user do
    resources :posts do
      resources :ratings
    end

    resources :users, only: [:show]
  end
end
